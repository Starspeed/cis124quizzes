$(document).ready(function(){
    var boxopened = "";
    var boxopened2 = "";
    var guesses = 1;
    var matches = 0;
    var click = 0;
    var image1 = "";
    var image2 = "";
    var normal = "../midterm/media/blank.png";
    
    $('.blocks').each(function () {        
        $(this).click(function () {
            if (click%2 === 0){
                boxopened = $(this);
                image1 = $(this).attr("title");
                $(this).css("background-image",'url(' + image1 + ')');
                console.log(image1);
                console.log(boxopened);
                click++;
            }
            else{
                boxopened2 = $(this);
                console.log(boxopened2);
                image2 = $(this).attr("title");
                console.log(image2);
                $(this).css("background-image",'url(' + image2 + ')');
                matching(boxopened, boxopened2);
                click++;
                guesses++;
                console.log(guesses);
            }

        });
        $("#restart").click(function(){
           location.reload(); 
        });
        
        function matching(pic, pic2){
            if (pic.attr('title') === pic2.attr('title')){
                matches++;  
                $("#guesscount").html("Number of Guesses: " +guesses);
                $("#matchcount").html("Number of Matches: " +matches);
                
            }
            
            else if (matches === 8){
                alert("You won!");
                $("body").css("font-family", "wing-dings");
            }
                
            else{
                (boxopened.add(boxopened2)).fadeOut(800, function(){
                    boxopened.css("background-image",'url(' + normal + ')');
                    boxopened2.css("background-image",'url(' + normal + ')');
                    (boxopened.add(boxopened2)).fadeIn("slow");
                });
                console.log("code=working?"); 
                $("#guesscount").html("Number of Guesses: " +guesses);

            }
                
        }
    
    });
        
});
